<?php

require_once 'config.php';


class Connection
{
	public static function make($dbHost, $dbName, $dbUser, $dbPassword, $dbPort)
	{
		$dsn = "mysql:host=$dbHost;dbname=$dbName;charset=UTF8;port=$dbPort";

		try {
			$options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];

			return new PDO($dsn, $dbUser, $dbPassword, $options);
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}
}

return Connection::make($dbHost, $dbName, $dbUser, $dbPassword, $dbPort);