<?php
     $pdo = require 'connect.php';

     $sql = "SELECT * FROM vols AS v LEFT JOIN meteo AS m ON m.volId = v.volId";
     $stmt = $pdo->query($sql);

     $pdo = null;
     $i = 0;

     while ($row = $stmt->fetch()) {
        if($i % 2 === 0) {
            $class = 'pair';
        } else {
            $class = 'impair';
        }
        echo   '<tr class = '.$class.'>
                    <td>'.$row['icao24'].'</td>
                    <td>'.$row['origin_country'].'</td>
                    <td>'.$row['city'].'</td>
                    <td>
                        <div class="bloc-logo-info">
                            <div class="bloc-logo">
                                <img src="./ressources/jour/'.$row['img'].'" alt="logo du temps qu\'il fait" class="logo-meteo">
                            </div>
                            <div class=\'bloc-info\'>
                                <p class=\'registered-flights-temperature\'>'.$row['temperature'].'°</p>
                            </div>
                        </div>
                    </td>
                </tr>';
        $i++;
     }

