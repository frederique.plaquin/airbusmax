<?php

const HTTP_OK = 200;
const HTTP_BAD_REQUEST = 400;
const HTTP_METHOD_NOT_ALLOWED = 405;


if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtoupper($_SERVER['HTTP_X_REQUESTED_WITH']) == 'XMLHTTPREQUEST') {
    // PREVENIR INJECTION ICI 
    $response_code = HTTP_BAD_REQUEST;
    $message = "Il manque le paramètre ACTION";

    if($_POST['action'] == 'showData') {
        $response_code = HTTP_OK;

        $message = '<h5>Requête reçue !</h5>';

        $pdo = require 'connect.php';

        $sql = "SELECT * FROM vols AS v LEFT JOIN meteo AS m ON m.volId = v.volId";
        $stmt = $pdo->query($sql);

        while ($row = $stmt->fetch()) {
            if($i % 2 === 0) {
                $class = 'pair';
            } else {
                $class = 'impair';
            }

            $icao = $row['icao24'];
            $origin = $row['origin_country'];
            $city = $row['city'];
            $img = $row['img'];
            $temperature = $row['temperature'];

            $tableContent = [$class, $icao, $origin, $city, $img, $temperature];

            $i++;
         }

        $pdoMessage = "<h5 id=\"info-user-titre\" style=\"font-family:\"PT Sans Narrow\", sans-serif; font-weight:100;font-size:20px;\">Le vol ".$flight['icao']." en provenance de " .$flight['origin_country']. " a bien été enregistré !</h5>";
    
        $pdo = null;
    }

    response($response_code, $message, $tableContent, $pdoMessage);
} else {
    $response_code = HTTP_METHOD_NOT_ALLOWED;
    $message = "Method not allowed!";

    response($response_code, $message);
}

function response($response_code, $response, $tableContent = null, $pdoMessage = null) {
    header('Content-Type: application/json');
    http_response_code($response_code);

    $response = [
        "response_code" => $response,
        "message" => $response_code,
        "tableContent" => $tableContent,
        "pdoMessage" => $pdoMessage
    ];

    echo json_encode($response);
}