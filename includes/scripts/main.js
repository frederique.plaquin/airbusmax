const CLEFAPI = 'be8139ccae300159e2367c6d11ddc497';
let resultatsAPI;

const CLEAPIMETEO = '52846f1b8a97e8d447f01ae0c941c90a'; /* 2e76c09ef2b2d5113867e0c49aec6a82 / f2d60e3491fa16d9730b6e325c0583ff / 52846f1b8a97e8d447f01ae0c941c90a*/
let resultatAPIMeteo;

let long;
let lat;
let buttonId;

const URL = "https://opensky-network.org/api/states/all?lamin=45.8389&lomin=5.9962&lamax=47.8229&lomax=10.5226";
const TITREMETEO = document.querySelector('.titre-left-content');
const IMG = document.querySelector('.bloc-logo');
const temps = document.querySelector('.temps');
const temperature = document.querySelector('.temperature');
const localisation = document.querySelector('.localisation');
const tbody = document.querySelector(('#body'));

let flights = [];
let table = document.getElementById("tableau");

// push flight in array
function getFlights(data) {
    console.log(data.states);
    data.states.forEach(element =>{
        flights.push(element)
    })
}

// get data from openweather api
function AppelApiMeteo(flight) {
    console.log('from apimeteo: click');

    let lattitude = flight[6];
    let longitude = flight[5];
    let URLMeteo = `https://api.openweathermap.org/data/2.5/onecall?lat=${lattitude}&lon=${longitude}&exclude=minutely&units=metric&lang=fr&appid=${CLEAPIMETEO}`;
    console.log(URLMeteo);
    fetch(URLMeteo)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            resultatAPIMeteo = data;

            TITREMETEO.innerHTML = `Météo du vol ${flight[1]} en provenance de ${flight[2]}`;
            IMG.innerHTML = `<img src="./ressources/jour/${resultatAPIMeteo.current.weather[0].icon}.svg" alt="logo du temps qu'il fait" className="logo-meteo">`;
            temps.innerText = resultatAPIMeteo.current.weather[0].description;
            temperature.innerText = `${Math.trunc(resultatAPIMeteo.current.temp)}°`;
            localisation.innerText = resultatAPIMeteo.timezone;

            console.log('meteo data:' + resultatAPIMeteo.current.weather[0].description);
        })
}

// register flight function
function RegisterFlight(flight, city) {
    console.log('from RegisterFlight: click' + flight[0]);
    let lattitude = flight[6];
    let longitude = flight[5];
    let URLMeteo = `https://api.openweathermap.org/data/2.5/onecall?lat=${lattitude}&lon=${longitude}&exclude=minutely&units=metric&lang=fr&appid=${CLEAPIMETEO}`;
    let post_data = [];

    // get data from openweather api
    fetch(URLMeteo)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            resultatAPIMeteo = data;

            let meteoImg = resultatAPIMeteo.current.weather[0].icon + '.svg';
            let meteoTemperature = `${Math.trunc(resultatAPIMeteo.current.temp)}°`;
            let meteoDescription = resultatAPIMeteo.current.weather[0].description;;

            // PREVENIR INJECTION ICI
            // post data to register in ajax
            $.ajax({
                type: "POST",
                url: "./includes/scripts/registerData.php",
                data: {
                    action: 'showData',
                    flight: {
                        icao: flight[0],
                        callsign: flight[1],
                        origin_country: flight[2],
                        time_position: flight[3],
                        last_contact: flight[4],
                        longitude: flight[5],
                        lattitude: flight[6],
                        city: city,
                        velocity: flight[9],
                        geo_altitude: flight[13]
                    },
                    meteo: {
                        image: meteoImg,
                        temperature: meteoTemperature,
                        description: meteoDescription
                    }
                },
                dataType: 'json',
                cache: false,
                success: function(response) {
                    const div = document.getElementById('info-user');
                    const titreDiv = document.getElementById('info-user-titre');
                    div.innerHTML = response.pdoMessage;
                    div.style.textAlign='center';
                    
                    console.log('json from php : ' + response.pdoMessage);
                },
                error: function(response) {
                    console.log('ERROR');
                },
                complete: function(response) {
                    console.log('COMPLETE');
                }
            });
        })
}

// update registered flights table
function UpdateRegisteredFlightTable() {
    // PREVENIR INJECTION ICI
            // post data to register in ajax
            $.ajax({
                type: "POST",
                url: "./includes/scripts/updateData.php",
                data: {
                    action: 'showData'
                },
                dataType: 'json',
                cache: false,
                success: function(response) {
                    console.log('json from updateTable : ' + response.tableContent);
                    const div = document.getElementById('registered-flight-body');

                    div.innerHTML = '';
                    let i = 0;

                    response.tableContent.forEach(row => {
                        let j = i+1;
                        const divContent = `<tr class = '${response.tableContent[i][j++]}'>
                                                <td>${response.tableContent[i][j++]}</td>
                                                <td>${response.tableContent[i][j++]}</td>
                                                <td>${response.tableContent[i][j++]}</td>
                                                <td>
                                                    <div class="bloc-logo-info">
                                                        <div class="bloc-logo">
                                                            <img src="./ressources/jour/${response.tableContent[i][j++]}" alt="logo du temps qu'il fait" class="logo-meteo">
                                                        </div>
                                                        <div class='bloc-info'>
                                                            <p class='registered-flights-temperature'>${response.tableContent[i][j++]}°</p>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>`;

                        div.innerHTML += divContent;
                        
                        i++;
                            
                    });
                },
                error: function(response) {
                    console.log('ERROR');
                },
                complete: function(response) {
                    console.log('COMPLETE');
                }
            });
}


// get data from opensky api
function AppelAPI() {
    fetch(URL)
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            let i = 0;

            getFlights(data);
            console.log(data)
            console.log(flights)
            
            // generate table row for each flights
            flights.forEach(flight => {
                let i = flights.indexOf(flight);
                console.log(flight);
                long = flight[5];
                lat = flight[6];

                let city = flight[5] + ', ' + flight[6];
                console.log("city: " + city);

                let newRow = document.createElement('tr');

                let newFlightNumber = document.createElement('td');
                let newflightNumberText = flight[0];

                let newProvenance = document.createElement('td');
                let newProvenanceText = flight[2];

                let newCity = document.createElement('td');
                let newCityText = city;

                let newButton = document.createElement('button');
                let newButtonText = 'Météo du vol';

                let newRegisterButton = document.createElement('button');
                let newRegisterButtonText = 'Enregistrer ce vol';


                tbody.append(newRow);
                if(i % 2 === 0) {
                    newRow.setAttribute('class', 'pair')
                } else {
                    newRow.setAttribute('class', 'impair')
                }

                newFlightNumber.textContent = newflightNumberText;
                newRow.append(newFlightNumber);

                newProvenance.textContent = newProvenanceText;
                newRow.append(newProvenance);

                newCity.textContent = newCityText;
                newRow.append(newCity);

                newButton.textContent = newButtonText;
                newRow.append(newButton);
                newButton.setAttribute('id', 'tr-'+i);
                newButton.setAttribute('class', 'meteo-button');

                newRegisterButton.textContent = newRegisterButtonText;
                newRow.append(newRegisterButton);
                newRegisterButton.setAttribute('id', 'regButton-'+i)
                newRegisterButton.setAttribute('class', 'register-button');

                console.log('tr-'+i);

                let button = document.getElementById('tr-'+i);

                // add eventlistener to meteo button
                button.addEventListener('click', (e) => {
                    console.log('from api: click');
                    AppelApiMeteo(flight);
                });

                //add event listener to register button
                let registerButton = document.getElementById('regButton-'+i);

                registerButton.addEventListener('click', (e) => {
                    console.log('from api reg: click');
                    RegisterFlight(flight, city);
                    setTimeout(UpdateRegisteredFlightTable, 1000);
                   
                })

                // add registered flights table content
                

                i++;
            })
        })
}

AppelAPI();