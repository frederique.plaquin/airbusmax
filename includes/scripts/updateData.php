<?php

const HTTP_OK = 200;
const HTTP_BAD_REQUEST = 400;
const HTTP_METHOD_NOT_ALLOWED = 405;


if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtoupper($_SERVER['HTTP_X_REQUESTED_WITH']) == 'XMLHTTPREQUEST') {
    // PREVENIR INJECTION ICI 
    $response_code = HTTP_BAD_REQUEST;
    $message = "Il manque le paramètre ACTION";

    if($_POST['action'] == 'showData') {
        $response_code = HTTP_OK;

        $message = '<h5>Requête reçue !</h5>';

        $pdo = require 'connect.php';

        $i = 1;

        $sql = "SELECT * FROM vols AS v INNER JOIN meteo AS m ON m.volId = v.volId";
        $stmt = $pdo->query($sql);

        foreach ($stmt as $row) {
            if($i % 2 === 0) {
                $class = 'pair';
            } else {
                $class = 'impair';
            }

            $icao = $row['icao24'];
            $origin = $row['origin_country'];
            $city = $row['city'];
            $img = $row['img'];
            $temperature = $row['temperature'];

            $row = array($i => $class, $icao, $origin, $city, $img, $temperature);

            $tableContent[] = $row;

            $i++;
         }

    }

    response($response_code, $message, $tableContent);
} else {
    $response_code = HTTP_METHOD_NOT_ALLOWED;
    $message = "Method not allowed!";

    response($response_code, $message);
}

function response($response_code, $response, $tableContent = null) {
    header('Content-Type: application/json');
    http_response_code($response_code);

    $response = [
        "response_code" => $response,
        "message" => $response_code,
        "tableContent" => $tableContent
    ];

    echo json_encode($response);
}