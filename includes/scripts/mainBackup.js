/*import {USERNAME,PASSWORD} from "../API/config.js";*/

const CLEFAPI = 'be8139ccae300159e2367c6d11ddc497';
let resultatsAPI;

const CLEAPIMETEO = '52846f1b8a97e8d447f01ae0c941c90a'; /* 2e76c09ef2b2d5113867e0c49aec6a82 / f2d60e3491fa16d9730b6e325c0583ff / 52846f1b8a97e8d447f01ae0c941c90a*/
let resultatAPIMeteo;

let long;
let lat;
let buttonId;

const URL = "https://opensky-network.org/api/states/all?lamin=45.8389&lomin=5.9962&lamax=47.8229&lomax=10.5226";
const TITREMETEO = document.querySelector('.titre-left-content');
const IMG = document.querySelector('.bloc-logo');
const temps = document.querySelector('.temps');
const temperature = document.querySelector('.temperature');
const localisation = document.querySelector('.localisation');
const tbody = document.querySelector(('#body'));

let flights = [];
let table = document.getElementById("tableau");


function getFlights(data) {
    console.log(data.states);
    data.states.forEach(element =>{
        flights.push(element)
    })
}

function AppelApiMeteo(flight) {
    console.log('from apimeteo: click');

    let lattitude = flight[6];
    let longitude = flight[5];
    let URLMeteo = `https://api.openweathermap.org/data/2.5/onecall?lat=${lattitude}&lon=${longitude}&exclude=minutely&units=metric&lang=fr&appid=${CLEAPIMETEO}`;
    console.log(URLMeteo);
    fetch(URLMeteo)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            resultatAPIMeteo = data;

            TITREMETEO.innerHTML = `Météo du vol ${flight[1]} en provenance de ${flight[2]}`;
            IMG.innerHTML = `<img src="./ressources/jour/${resultatAPIMeteo.current.weather[0].icon}.svg" alt="logo du temps qu'il fait" className="logo-meteo">`;
            temps.innerText = resultatAPIMeteo.current.weather[0].description;
            temperature.innerText = `${Math.trunc(resultatAPIMeteo.current.temp)}°`;
            localisation.innerText = resultatAPIMeteo.timezone;

            console.log('meteo data:' + resultatAPIMeteo.current.weather[0].description);
        })
}

function RegisterFlight(flight, city) {
    console.log('from RegisterFlight: click' + flight[0]);
    let lattitude = flight[6];
    let longitude = flight[5];
    let URLMeteo = `https://api.openweathermap.org/data/2.5/onecall?lat=${lattitude}&lon=${longitude}&exclude=minutely&units=metric&lang=fr&appid=${CLEAPIMETEO}`;
    let post_data = [];

    fetch(URLMeteo)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            resultatAPIMeteo = data;

            let meteoImg = resultatAPIMeteo.current.weather[0].icon + '.svg';
            let meteoTemperature = `${Math.trunc(resultatAPIMeteo.current.temp)}°`;
            let meteoDescription = resultatAPIMeteo.current.weather[0].description;;

            // PREVENIR INJECTION ICI
            $.ajax({
                type: "POST",
                url: "./includes/scripts/registerData.php",
                data: {
                    action: 'showData',
                    flight: {
                        icao: flight[0],
                        callsign: flight[1],
                        origin_country: flight[2],
                        time_position: flight[3],
                        last_contact: flight[4],
                        longitude: flight[5],
                        lattitude: flight[6],
                        city: city,
                        velocity: flight[9],
                        geo_altitude: flight[13]
                    },
                    meteo: {
                        image: meteoImg,
                        temperature: meteoTemperature,
                        description: meteoDescription
                    }
                },
                dataType: 'json',
                cache: false,
                success: function(response) {
                    const div = document.getElementById('info-user');
                    const titreDiv = document.getElementById('info-user-titre');
                    div.innerHTML = response.pdoMessage;
                    div.style.textAlign='center';
                    
                    console.log('json from php : ' + response.pdoMessage);
                },
                error: function(response) {
                    console.log('ERROR');
                },
                complete: function(response) {
                    console.log('COMPLETE');
                }
            });
        })
}

function AppelAPI() {
    fetch(URL)
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            let i = 0;

            getFlights(data);
            console.log(data)
            console.log(flights)

            flights.forEach(flight => {
                let i = flights.indexOf(flight)
                console.log(flight);

                fetch(`https://api.opencagedata.com/geocode/v1/json?q=${flight[6]}+${flight[5]}&key=6d0e711d72d74daeb2b0bfd2a5cdfdba`)
                    .then((response) => {
                        return response.json();
                    })
                    .then ((data) => {
                        console.log(data);
                        console.log(data.results);
                        console.log(data.results[0].components['city']);

                        let city = data.results[0].components['city'];

                        let newRow = document.createElement('tr');

                        let newFlightNumber = document.createElement('td');
                        let newflightNumberText = flight[0];

                        let newProvenance = document.createElement('td');
                        let newProvenanceText = flight[2];

                        let newCity = document.createElement('td');
                        let newCityText = city;

                        let newButton = document.createElement('button');
                        let newButtonText = 'Météo du vol';

                        let newRegisterButton = document.createElement('button');
                        let newRegisterButtonText = 'Enregistrer ce vol';


                        tbody.append(newRow);
                        if(i % 2 === 0) {
                            newRow.setAttribute('class', 'pair')
                        } else {
                            newRow.setAttribute('class', 'impair')
                        }

                        newFlightNumber.textContent = newflightNumberText;
                        newRow.append(newFlightNumber);

                        newProvenance.textContent = newProvenanceText;
                        newRow.append(newProvenance);

                        newCity.textContent = newCityText;
                        newRow.append(newCity);

                        newButton.textContent = newButtonText;
                        newRow.append(newButton);
                        newButton.setAttribute('id', 'tr-'+i);
                        newButton.setAttribute('class', 'meteo-button');

                        newRegisterButton.textContent = newRegisterButtonText;
                        newRow.append(newRegisterButton);
                        newRegisterButton.setAttribute('id', 'regButton-'+i)
                        newRegisterButton.setAttribute('class', 'register-button');

                        console.log('tr-'+i);

                        let button = document.getElementById('tr-'+i);

                        button.addEventListener('click', (e) => {
                            console.log('from api: click');
                            AppelApiMeteo(flight);
                        });

                        let registerButton = document.getElementById('regButton-'+i);


                        registerButton.addEventListener('click', (e) => {
                            console.log('from api reg: click');
                            RegisterFlight(flight, city);
                        })
                        i++;
                    })
            })
        })
}

AppelAPI();
/*
let row = document.createElement('tr')
let columnNum = document.createElement('th');
let columnLat = document.createElement('th');
let columnLong = document.createElement('th');
let columnMeteo = document.createElement('th')

for(let i = 0; i < data.length; i++) {

    columnNum.textContent = resultatsAPI[i].icao24;

    table.appendChild(row);
    row.appendChild(columnNum);
    row.appendChild(columnLat);
    row.appendChild(columnLong);
    row.appendChild(columnMeteo);


    console.log('test:' + resultatsAPI[i].icao24);
}
*/
/*import {USERNAME,PASSWORD} from "../API/config.js";*/

const CLEFAPI = 'be8139ccae300159e2367c6d11ddc497';
let resultatsAPI;

const CLEAPIMETEO = '52846f1b8a97e8d447f01ae0c941c90a'; /* 2e76c09ef2b2d5113867e0c49aec6a82 / f2d60e3491fa16d9730b6e325c0583ff / 52846f1b8a97e8d447f01ae0c941c90a*/
let resultatAPIMeteo;

let long;
let lat;
let buttonId;

const URL = "https://opensky-network.org/api/states/all?lamin=45.8389&lomin=5.9962&lamax=47.8229&lomax=10.5226";
const TITREMETEO = document.querySelector('.titre-left-content');
const IMG = document.querySelector('.bloc-logo');
const temps = document.querySelector('.temps');
const temperature = document.querySelector('.temperature');
const localisation = document.querySelector('.localisation');
const tbody = document.querySelector(('#body'));

let flights = [];
let table = document.getElementById("tableau");


function getFlights(data) {
    console.log(data.states);
    data.states.forEach(element =>{
        flights.push(element)
    })
}

function getLocation(flight){
    let lattitude = flight[6];
    let longitude = flight[5];

    fetch(`https://geocodeapi.p.rapidapi.com/GetNearestCities?range=0&longitude=${longitude}&latitude=${lattitude}`, {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "geocodeapi.p.rapidapi.com",
            "x-rapidapi-key": "3c0b61ccebmshabd248762571850p1079fejsn2feaa89c922b"
        }
    })
        let geocoder;
        geocoder = new google.maps.Geocoder();
        let latlng = {
            lat: parseFloat(lattitude),
            lng: parseFloat(longitude),
        };

        geocoder
            .geocode({location: latlng})
            .then((response) => {
                console.log("pos: " + response);
                console.log("pos: " + response.results[7].address_components[0].long_name);
            })
            .catch((e) => window.alert("Geocoder failed due to: " + e));
}

function AppelApiMeteo(flight) {
    console.log('from apimeteo: click');

    let lattitude = flight[6];
    let longitude = flight[5];
    let URLMeteo = `https://api.openweathermap.org/data/2.5/onecall?lat=${lattitude}&lon=${longitude}&exclude=minutely&units=metric&lang=fr&appid=${CLEAPIMETEO}`;
    console.log(URLMeteo);
    fetch(URLMeteo)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            resultatAPIMeteo = data;

            TITREMETEO.innerHTML = `Météo du vol ${flight[1]} en provenance de ${flight[2]}`;
            IMG.innerHTML = `<img src="./ressources/jour/${resultatAPIMeteo.current.weather[0].icon}.svg" alt="logo du temps qu'il fait" className="logo-meteo">`;
            temps.innerText = resultatAPIMeteo.current.weather[0].description;
            temperature.innerText = `${Math.trunc(resultatAPIMeteo.current.temp)}°`;
            localisation.innerText = resultatAPIMeteo.timezone;

            console.log('meteo data:' + resultatAPIMeteo.current.weather[0].description);
        })
}

function RegisterFlight(flight, city) {
    console.log('from RegisterFlight: click' + flight[0]);
    let lattitude = flight[6];
    let longitude = flight[5];
    let URLMeteo = `https://api.openweathermap.org/data/2.5/onecall?lat=${lattitude}&lon=${longitude}&exclude=minutely&units=metric&lang=fr&appid=${CLEAPIMETEO}`;
    let post_data = [];

    fetch(URLMeteo)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            resultatAPIMeteo = data;

            let meteoImg = resultatAPIMeteo.current.weather[0].icon + '.svg';
            let meteoTemperature = `${Math.trunc(resultatAPIMeteo.current.temp)}°`;
            let meteoDescription = resultatAPIMeteo.current.weather[0].description;;

            // PREVENIR INJECTION ICI
            $.ajax({
                type: "POST",
                url: "./includes/scripts/registerData.php",
                data: {
                    action: 'showData',
                    flight: {
                        icao: flight[0],
                        callsign: flight[1],
                        origin_country: flight[2],
                        time_position: flight[3],
                        last_contact: flight[4],
                        longitude: flight[5],
                        lattitude: flight[6],
                        city: city,
                        velocity: flight[9],
                        geo_altitude: flight[13]
                    },
                    meteo: {
                        image: meteoImg,
                        temperature: meteoTemperature,
                        description: meteoDescription
                    }
                },
                dataType: 'json',
                cache: false,
                success: function(response) {
                    const div = document.getElementById('info-user');
                    const titreDiv = document.getElementById('info-user-titre');
                    div.innerHTML = response.pdoMessage;
                    div.style.textAlign='center';
                    
                    console.log('json from php : ' + response.pdoMessage);
                },
                error: function(response) {
                    console.log('ERROR');
                },
                complete: function(response) {
                    console.log('COMPLETE');
                }
            });
        })
}

function AppelAPI() {
    fetch(URL)
        .then((response) => {
            return response.json()
        })
        .then((data) => {
            let i = 0;

            getFlights(data);
            console.log(data)
            console.log(flights)

            flights.forEach(flight => {
                let i = flights.indexOf(flight)
                console.log(flight);
                long = flight[5];
                lat = flight[6];

                let city = getLocation(flight);
                console.log("city: " + city);

                let newRow = document.createElement('tr');

                let newFlightNumber = document.createElement('td');
                let newflightNumberText = flight[0];

                let newProvenance = document.createElement('td');
                let newProvenanceText = flight[2];

                let newCity = document.createElement('td');
                let newCityText = city;

                let newButton = document.createElement('button');
                let newButtonText = 'Météo du vol';

                let newRegisterButton = document.createElement('button');
                let newRegisterButtonText = 'Enregistrer ce vol';


                tbody.append(newRow);
                if(i % 2 === 0) {
                    newRow.setAttribute('class', 'pair')
                } else {
                    newRow.setAttribute('class', 'impair')
                }

                newFlightNumber.textContent = newflightNumberText;
                newRow.append(newFlightNumber);

                newProvenance.textContent = newProvenanceText;
                newRow.append(newProvenance);

                newCity.textContent = newCityText;
                newRow.append(newCity);

                newButton.textContent = newButtonText;
                newRow.append(newButton);
                newButton.setAttribute('id', 'tr-'+i);
                newButton.setAttribute('class', 'meteo-button');

                newRegisterButton.textContent = newRegisterButtonText;
                newRow.append(newRegisterButton);
                newRegisterButton.setAttribute('id', 'regButton-'+i)
                newRegisterButton.setAttribute('class', 'register-button');

                console.log('tr-'+i);

                let button = document.getElementById('tr-'+i);

                button.addEventListener('click', (e) => {
                    console.log('from api: click');
                    AppelApiMeteo(flight);
                });

                let registerButton = document.getElementById('regButton-'+i);


                registerButton.addEventListener('click', (e) => {
                    console.log('from api reg: click');
                    RegisterFlight(flight, city);
                })
                i++;
            })
        })
}

AppelAPI();