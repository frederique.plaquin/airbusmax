<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Airbus Max</title>
    <link rel="stylesheet" href="./includes/css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Hind:wght@300;400;500;600;700&family=PT+Sans+Narrow:wght@400;700&family=PT+Sans:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
    <header>
        <h1>Info-vols : Le site de l'info des vols !</h1>
        <h2>Besoin d'informations au sujet d'un vol? Vous êtes au bon endroit!</h2>
    </header>

    <main>
        <div id="flex-content">
            <div class="pretty-line"></div>
            <div id="info-user"></div>
            <div id="tableau-des-vols-container">
                <div id="tableau-des-vols">
                    <div id="flex-display">
                        <div id="liste-vols-container">
                            <h2>Liste des vols</h2>
                            <!-- POSSIBLE INJECTION ICI -->
                            <div id="formulaire">
                                <form>
                                    <label>Chercher par numéro de vol: </label><input>
                                </form>
                            </div>
                            <table id="tableau">
                                <thead>
                                    <tr>
                                        <th id="num">Numéro de vol</th>
                                        <th id="lat">Provenance</th>
                                        <th id="long">Position actuelle</th>
                                        <th id="meteo"></th>
                                        <th id="register"></th>
                                    </tr>
                                </thead>
                                <tbody id="body">
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="left-content">
                            <div id="meteo-display">
                                <h2 class="titre-left-content">Sélectionnez un avion pour afficher sa météo !</h2>
                                <div class="bloc-logo-info">
                                    <div class="bloc-logo"></div>
                                    <div class="bloc-info">
                                        <p class="temps"></p>
                                        <p class="temperature"></p>
                                        <p class="localisation"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="pretty-line"></div>
                            <div id="registered-flights-display">
                                <h2 class="titre-left-content">Liste des vols enregistrés</h2>
                                <table id="tableau-registered-flights">
                                    <thead>
                                        <tr>
                                            <th id="registered-flight-num">Numéro de vol</th>
                                            <th id="registered-flight-lat">Provenance</th>
                                            <th id="registered-flight-long">Position enregistrée</th>
                                            <th id="registered-flight-meteo">Meteo</th>
                                        </tr>
                                    </thead>
                                    <tbody id="registered-flight-body">
                                            <?php 
                                                include_once('./includes/scripts/getData.php');
                                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <footer>

    </footer>

    <script type="module" src="includes/scripts/main.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCOK0Q1fbQOWFCuumTw9uc3G05WKFD9zdY"></script>
</body>
</html>